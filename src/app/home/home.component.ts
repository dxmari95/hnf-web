import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services';
declare var $, google;
@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    filterDataList: any = {};
    homeDetails: any = {};
    searchVal = '';
    email = "";
    forumsCategory = [];
    currForumCategory: any = {};
    searchResults = {
        doctors: [],
        fitness: [],
        hospitals: [],
        pharmacies: [],
        shops: [],
    }
    constructor(public common: GlobalService) {
        this.common.loaderShow();
    }

    ngOnInit() {
        if (this.common.authData) {
            this.listFilterData();
        }

        this.listForumCategories();

        this.initJS();
        this.retrieveDetails();
    }

    retrieveDetails() {
        this.search().then(res => {
            this.common.loaderHide();
            if (res) {
                this.homeDetails = res;
            }
            this.common.loaderHide();
            setTimeout(() => {
                this.initSlickCarousel();
            }, 500);
        })
    }

    listForumCategories() {
        this.common.getService(this.common.config.FORUMSCATEGORY).then((res: any) => {
            this.forumsCategory = res;
        }).catch(err => {
        });
    }

    showHideAutoComplete(isShow) {
        $(".qty-dropdown-content.search_block").slideToggle(400);
    }

    selectCategory(category) {
        this.common.go('forums', category.id)
    }

    search(search = '') {
        return new Promise((resolve) => {
            this.common.getService(this.common.config.HOMEPAGE + "?q=" + search).then((res: any) => {
                this.searchResults = {
                    doctors: res.search_doctors,
                    fitness: res.search_fitness,
                    hospitals: res.search_hospitals,
                    pharmacies: res.search_pharmacies,
                    shops: res.search_shops
                }
                console.log(this.searchResults);
                resolve(res);
            }).catch(err => {
                resolve(false);
                console.log(err);
            })
        })
    }

    initSlickCarousel() {
        $('.light-carousel-1').slick({
            infinite: true,
            slidesToShow: 2,
            dots: false,
            arrows: false,
            centerMode: false,
            responsive: [{
                breakpoint: 1224,
                settings: {
                    slidesToShow: 2,
                    centerMode: false,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '0',
                }
            }
            ]
        });
        $(".lc-prev").on("click", function () {
            $(this).closest(".light-carousel-wrap").find('.light-carousel-1').slick('slickPrev');
        });
        $(".lc-next").on("click", function () {
            $(this).closest(".light-carousel-wrap").find('.light-carousel-1').slick('slickNext');
        });

        $('.listing-carousel-1').slick({
            infinite: true,
            slidesToShow: 3,
            dots: true,
            arrows: false,
            centerMode: true,
            centerPadding: '60px',
            responsive: [{
                breakpoint: 1224,
                settings: {
                    slidesToShow: 3,
                }
            },

            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,

                }
            },
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 1,
                    centerPadding: '0',
                }
            }
            ]

        });
        var sbp = $('.swiper-button-prev'),
            sbn = $('.swiper-button-next');

        sbp.on("click", function () {
            $(this).closest(".list-carousel").find('.listing-carousel-1').slick('slickPrev');
        });
        sbn.on("click", function () {
            $(this).closest(".list-carousel").find('.listing-carousel-1').slick('slickNext');
        });

        $('.single-carousel-1').slick({
            infinite: true,
            slidesToShow: 3,
            dots: true,
            arrows: false,
            centerMode: true,
            responsive: [{
                breakpoint: 1224,
                settings: {
                    slidesToShow: 2,
                    centerMode: false,
                }
            },

            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '0',
                }
            }
            ]

        });
        sbp.on("click", function () {
            $(this).closest(".slider-carousel-wrap").find('.single-carousel-1').slick('slickPrev');
        });
        sbn.on("click", function () {
            $(this).closest(".slider-carousel-wrap").find('.single-carousel-1').slick('slickNext');
        });
    }

    initJS() {
        setTimeout(() => {
            var a = $(".bg");
            a.each(function (a) {
                if ($(this).attr("data-bg")) $(this).css("background-image", "url(" + $(this).data("bg") + ")");
            });
        }, 500);
    }

    goFitnessDetail(idx) {
        let id = this.homeDetails.fitness[idx].id;
        this.common.go('fitness/' + id);
    }

    initSlick() {
        function initAutocomplete() {
            var acInputs = document.getElementsByClassName("autocomplete-input");
            for (var i = 0; i < acInputs.length; i++) {
                var autocomplete = new google.maps.places.Autocomplete(acInputs[i]);
                autocomplete.inputId = acInputs[i].id;
            }
        }
        var sbp = $('.swiper-button-prev'),
            sbn = $('.swiper-button-next');
        $('.fw-carousel').slick({
            dots: true,
            infinite: true,
            speed: 600,
            slidesToShow: 1,
            centerMode: false,
            arrows: false,
            variableWidth: true
        });
        sbp.on("click", function () {
            $('.fw-carousel').slick('slickPrev');
        })

        sbn.on("click", function () {
            $('.fw-carousel').slick('slickNext');
        })
        $('.slideshow-container').slick({
            dots: true,
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            arrows: false,
            fade: true,
            cssEase: 'ease-in',
            infinite: true,
            speed: 1000,
        });
        $('.single-slider').slick({
            infinite: true,
            slidesToShow: 1,
            dots: true,
            arrows: false,
            adaptiveHeight: true
        });
        sbp.on("click", function () {
            $(this).closest(".single-slider-wrapper").find('.single-slider').slick('slickPrev');
        });
        sbn.on("click", function () {
            $(this).closest(".single-slider-wrapper").find('.single-slider').slick('slickNext');
        });
        $('.slider-container').slick({
            infinite: true,
            slidesToShow: 1,
            dots: true,
            arrows: false,
            adaptiveHeight: true,
        });
        $('.slider-container').on('init', function (event, slick) {
            initAutocomplete();
        });
        sbp.on("click", function () {
            $(this).closest(".slider-container-wrap").find('.slider-container').slick('slickPrev');

        });
        sbn.on("click", function () {
            $(this).closest(".slider-container-wrap").find('.slider-container').slick('slickNext');
        });
        $('.single-carousel').slick({
            infinite: true,
            slidesToShow: 3,
            dots: true,
            arrows: false,
            centerMode: true,
            responsive: [{
                breakpoint: 1224,
                settings: {
                    slidesToShow: 2,
                    centerMode: false,
                }
            },

            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '0',
                }
            }
            ]

        });
        sbp.on("click", function () {
            $(this).closest(".slider-carousel-wrap").find('.single-carousel').slick('slickPrev');
        });
        sbn.on("click", function () {
            $(this).closest(".slider-carousel-wrap").find('.single-carousel').slick('slickNext');
        });
        $('.inline-carousel').slick({
            infinite: true,
            slidesToShow: 3,
            dots: true,
            arrows: false,
            centerMode: false,
            responsive: [{
                breakpoint: 1224,
                settings: {
                    slidesToShow: 4,
                    centerMode: false,
                }
            },

            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                }
            }
            ]
        });
        $(".fc-cont-prev").on("click", function () {
            $(this).closest(".inline-carousel-wrap").find('.inline-carousel').slick('slickPrev');
        });
        $(".fc-cont-next").on("click", function () {
            $(this).closest(".inline-carousel-wrap").find('.inline-carousel').slick('slickNext');
        });
        $('.footer-carousel').slick({
            infinite: true,
            slidesToShow: 5,
            dots: false,
            arrows: false,
            centerMode: false,
            responsive: [{
                breakpoint: 1224,
                settings: {
                    slidesToShow: 4,
                    centerMode: false,
                }
            },

            {
                breakpoint: 568,
                settings: {
                    slidesToShow: 3,
                    centerMode: false,
                }
            }
            ]

        });
        $(".fc-cont-prev").on("click", function () {
            $(this).closest(".footer-carousel-wrap").find('.footer-carousel').slick('slickPrev');
        });
        $(".fc-cont-next").on("click", function () {
            $(this).closest(".footer-carousel-wrap").find('.footer-carousel').slick('slickNext');
        });
        $('.listing-carousel').slick({
            infinite: true,
            slidesToShow: 4,
            dots: true,
            arrows: false,
            centerMode: true,
            centerPadding: '60px',
            responsive: [{
                breakpoint: 1224,
                settings: {
                    slidesToShow: 3,
                }
            },

            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,

                }
            },
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 1,
                    centerPadding: '0',
                }
            }
            ]

        });
        sbp.on("click", function () {
            $(this).closest(".list-carousel").find('.listing-carousel').slick('slickPrev');
        });
        sbn.on("click", function () {
            $(this).closest(".list-carousel").find('.listing-carousel').slick('slickNext');
        });
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: false,
            dots: true,
            asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            arrows: false,
            centerMode: true,
            focusOnSelect: true
        });
        sbp.on("click", function () {
            $(this).closest(".single-slider-wrapper").find('.slider-for').slick('slickPrev');
        });
        sbn.on("click", function () {
            $(this).closest(".single-slider-wrapper").find('.slider-for').slick('slickNext');
        });
    }

    listFilterData() {
        this.common.getService(this.common.config.LISTFILTERDATA).then((res: any) => {
            this.filterDataList = res;
        }).catch(err => {
            console.log(err);
        })
    }

    subscribe(email) {
        if(!this.common.validateEmail(email)){
            $('.subscribe-message').html(`<i class="fa fa-warning"></i> You must enter a valid e-mail address.`);
            // this.common.showErrorToast("Please enter a valid email address");
            return;
        }
        $('.subscribe-message').html("Submitting...")
        this.common.postService(this.common.config.NEWSLETTER, {
            email: email
        }).then(() => {
            $('.subscribe-message').html(`<i class="fa fa-check"></i> You have been successfully subscribed.`);
            // this.common.showSuccessToast("You have been successfully subscribed.");
        }).catch(err => {
            console.log(err);
            this.common.showErrorToast('', err)
        })
    }

}
