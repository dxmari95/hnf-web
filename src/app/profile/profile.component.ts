import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services';
declare var $;
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profileDetails: any = {
    address: {}
  };
  countriesList = [];
  profileValidator: any = {};
  addressValidator: any = {};
  passwordDetails: any = {};
  passwordValidator: any = {};
  likesList: any = {};
  currTab = 1;
  constructor(public common: GlobalService) {
    this.common.loaderShow();
  }

  ngOnInit() {
    if (!this.common.authData) {
      this.common.go('/');
    }
    this.listCountries();
    setTimeout(() => {
      $('#gender').niceSelect();
      this.initJs();
    }, 500);
    this.retrieveProfile();
    this.getDoctorsLikes(1);
  }

  getDoctorsLikes(type) {
    return new Promise((resolve, reject) => {
      let url = "";
      if (type == 1) {
        url = this.common.config.MYLIKESDOCTOR;
      } else if (type == 2) {
        url = this.common.config.MYLIKESCLINICS;
      } else if (type == 3) {
        url = this.common.config.MYLIKESPHARMACIES;
      } else if (type == 4) {
        url = this.common.config.MYLIKESSHOPS;
      } else if (type == 5) {
        url = this.common.config.MYLIKESFITNESS;
      } else if (type == 6) {
        url = this.common.config.MYLIKESBLOGS;
      } else if (type == 7) {
        url = this.common.config.MYLIKESVIDEOS;
      } else if (type == 8) {
        url = this.common.config.MYLIKESFLIPBOOKS;
      }
      this.common.getService(url).then(res => {
        console.log(res);
        this.likesList = res;
      });
    });
  }

  showBookmarkList(type) {
    console.log(type);
    this.currTab = type;
    this.getDoctorsLikes(type);
  }

  showcardlist(type) {
    console.log(type);
    this.currTab = type;
    this.getDoctorsLikes(type);
  }

  initJs() {
    var sideTabsPaneHeight = function () {
      var navHeight = $('.nav-tabs.nav-tabs-left').outerHeight() || $('.nav-tabs.nav-tabs-right').outerHeight() || 0;

      var paneHeight = 0;

      $('.custom-tab-content.side-tabs .tab-pane').each(function (idx) {
        paneHeight = $(this).outerHeight() > paneHeight ? $(this).outerHeight() : paneHeight;
      });

      $('.custom-tab-content.side-tabs .tab-pane').each(function (idx) {
        $(this).css('min-height', navHeight + 'px');
      });
    };

    $(function () {
      sideTabsPaneHeight();

      $(window).resize(function () {
        sideTabsPaneHeight();
      });

      $('.nav-tabs.nav-tabs-left').resize(function () {
        sideTabsPaneHeight();
      });

      $('.image-upload-wrap').bind('dragover', function () {
        $('.image-upload-wrap').addClass('image-dropping');
      });
      $('.image-upload-wrap').bind('dragleave', function () {
        $('.image-upload-wrap').removeClass('image-dropping');
      });
    });

    $(document).ready(() => {
      $('select').niceSelect();
      $('#date_picker').bootstrapMaterialDatePicker({
        date: true,
        time: true,
        clearButton: true
      });
      $('#date_picker1').bootstrapMaterialDatePicker({
        time: false,
        date: true,
        clearButton: true
      });

      $('#profile_pic').change((e) => {
        console.log(e);
        this.readURL(e.target);
        this.updateProfilePic();
      });
    });

    this.profileValidator = $("#form").validate({
      rules: {
        first_name: {
          required: true
        },
        last_name: {
          required: true
        },
        username: {
          required: true
        },
        email: {
          required: true
        },
        date: {
          required: true
        }
      },
      errorElement: "div",
      errorPlacement: function (error, element) {
        var placement = $(element).data("error");
        if (placement) {
          $(placement).append(error);
        } else {
          error.insertAfter(element);
        }
      },
      submitHandler: e => {
        console.log(e);
        setTimeout(() => {
          this.updateProfileDetails();
        }, 500);
        return false;
      }
    });

    this.addressValidator = $("#form2").validate({
      rules: {
        address_line_1: {
          required: true
        },
        address_line_2: {
          required: true
        },
        city: {
          required: true
        },
        state: {
          required: true
        },
        postcode: {
          required: true
        }
      },
      errorElement: "div",
      errorPlacement: function (error, element) {
        var placement = $(element).data("error");
        if (placement) {
          $(placement).append(error);
        } else {
          error.insertAfter(element);
        }
      },
      submitHandler: e => {
        console.log(e);
        setTimeout(() => {
          this.updateAddressDetails();
        }, 500);
        return false;
      }
    });

    this.passwordValidator = $("#form3").validate({
      rules: {
        old_password: {
          required: true,
          minlength: 6
        },
        new_password: {
          required: true,
          minlength: 6
        },
        confirm_password: {
          required: true,
          minlength: 6,
          equalTo: "#new_password"
        }
      },
      //For custom messages
      messages: {
        old_password: {
          minlength: "Please enter at least 6 characters"
        },

        new_password: {
          minlength: "Please enter at least 6 characters"
        },
        confirm_password: {
          minlength: "Please enter at least 6 characters",
          equalTo: "Passwords does not match"
        }
      },
      errorElement: "div",
      errorPlacement: function (error, element) {
        var placement = $(element).data("error");
        if (placement) {
          $(placement).append(error);
        } else {
          error.insertAfter(element);
        }
      },
      submitHandler: e => {
        console.log(e);
        setTimeout(() => {
          this.changePassword();
        }, 500);
        return false;
      }
    });
  }

  retrieveProfile() {
    this.common.getService(this.common.config.PROFILE).then((res: any) => {
      if (!res.userprofile) {
        res.userprofile = {};
      }
      this.profileDetails = {
        "full_name": res.first_name + ' ' + res.last_name,
        "first_name": res.first_name,
        "last_name": res.last_name,
        "email": res.email,
        "username": res.username,
        "userprofile": {
          "dob": res.userprofile.dob,
          "gender": res.userprofile.gender,
          "profile_pic": res.userprofile.profile_pic
        },
        "address": res.address || {
          "address_line_1": "",
          "address_line_2": "",
          "city": "",
          "state": "",
          "country": "",
          "postcode": ""
        },
      }
      console.log(res);
      if (this.profileDetails.userprofile) {
        $('#gender').val(this.profileDetails.userprofile.gender);
        $('#gender').niceSelect('update');
      }

      if (this.profileDetails.address.country) {
        var interval = setInterval(() => {
          if (this.countriesList.length > 0) {
            clearInterval(interval);
            $('#country').val(this.profileDetails.address.country);
            $('#country').niceSelect('update');
          }
        }, 500);
      }

      if (this.profileDetails.userprofile.dob) {
        $('#date_picker1').val(this.profileDetails.userprofile.dob);
      }
      if (this.profileDetails.userprofile.profile_pic) {
        $('.image-upload-wrap').hide();
        $('.file-upload-image').attr('src', this.profileDetails.userprofile.profile_pic);
        $('.file-upload-content').show();
      }
      this.common.loaderHide();
    }).catch(err => {
      console.log(err);
      this.common.loaderHide();
    })
  }

  readURL(input) {
    if (input.files && input.files[0]) {

      var reader = new FileReader();

      reader.onload = function (e: any) {
        $('.image-upload-wrap').hide();

        $('.file-upload-image').attr('src', e.target.result);
        $('.file-upload-content').show();

        $('.image-title').html(input.files[0].name);
      };

      reader.readAsDataURL(input.files[0]);

    } else {
      this.removeUpload();
    }
  }

  removeUpload() {
    $('.file-upload-input').replaceWith($('.file-upload-input').clone());
    $('.file-upload-content').hide();
    $('.image-upload-wrap').show();
  }

  listCountries() {
    return new Promise((resolve, reject) => {
      this.common.getService(this.common.config.COUNTRIES).then((res: any) => {
        this.countriesList = res.country;
        resolve(res);
        setTimeout(() => {
          $('#country').niceSelect();
        }, 200);
      }).catch(err => {
        reject(err);
      })
    });
  }

  updateProfileDetails() {
    let query = {
      "first_name": this.profileDetails.first_name,
      "last_name": this.profileDetails.last_name,
      "email": this.profileDetails.email,
      "userprofile": {
        "dob": $('#date_picker1').val(),
        "gender": $('#gender').val()
      }
    }
    this.common.patchService(this.common.config.UPDATEPROFILE, query).then(res => {
      console.log(res);
      this.retrieveProfile();
      this.common.showSuccessToast("Profile was updated successfully.");
    }).catch(err => {
      console.log(err);
      this.common.showErrorToast('', err);
    });
  }
  updateAddressDetails() {
    this.profileDetails.address.country = $('#country').val();
    if (!this.profileDetails.address.country) {
      return;
    }
    this.common.postService(this.common.config.UPDATEPROFILEADDRESS, this.profileDetails.address).then(res => {
      console.log(res);
      this.common.showSuccessToast("Address was updated successfully.");
    }).catch(err => {
      console.log(err);
      this.common.showErrorToast('', err);
    });
  }
  updateProfilePic() {
    let file;
    if ($('#profile_pic')[0].files.length > 0) {
      file = $('#profile_pic')[0].files[0];
      let query = this.common.jsonToFormdata({
        profile_pic: file
      })
      this.common.patchFormService(this.common.config.UPDATEPROFILEPICTURE, query).then(res => {
        console.log(res);
        this.retrieveProfile();
        this.common.showSuccessToast("Profile Picture was updated successfully.");
      }).catch(err => {
        console.log(err);
        this.common.showErrorToast('', err);
      });
    }
  }

  changePassword() {
    let query = {
      "current_password": this.passwordDetails.old_password,
      "new_password": this.passwordDetails.new_password
    };
    this.common.postService(this.common.config.CHANGEPWD, query).then(res => {
      console.log(res);
      this.passwordDetails = {};
      this.common.showSuccessToast("Password was updated successfully.");
    }).catch(err => {
      console.log(err);
      this.common.showErrorToast('', err);
    });
  }

  logout() {
    this.common.logout()
  }

  goEditPass() {
    let scrollTop = $('#form3').offset().top;
    $('#old_password').focus();
  }
}
