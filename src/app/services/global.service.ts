import { Injectable, HostListener } from "@angular/core";
import { Http, RequestOptions, Headers, Response } from "@angular/http";
import { Toast, ToasterService, ToasterConfig } from "angular2-toaster";
import { UUID } from "angular2-uuid";
declare var moment, MarkerClusterer, google, cardRaining, InfoBox;

import { GoogleService, FacebookService } from "social-login-oauth";
const gClientid = "703840183543-arv57r1mc95d1toi2n3go63lepsj67nf.apps.googleusercontent.com";
const fAppId = "2403005953143794";

import {
  Router,
  ActivatedRoute,
  NavigationStart,
  NavigationEnd
} from "@angular/router";
import { DatePipe } from "@angular/common";
import { Title } from "@angular/platform-browser";
declare var $, google;

interface Locations {
  navigateTo: string;
  name: string;
  image: string;
  address: string;
  title: string;
  reviews_count: number;
  index: number;
  lat: string;
  lng: string;
}

const APIURL = "http://api-hnf.cruzbytes.com/"; //live(Dev)
// const APIURL = "http://api.questoh.com/"; //live(Prod)

@Injectable()
export class GlobalService {
  data = [];
  headers: Headers;
  loading;
  userIP;
  config = {
    // Auth module
    LOGIN: APIURL + "auth/user/login",
    REGISTER: APIURL + "auth/user/register",
    ACTIVATEACCOUNT: APIURL + "auth/user/activate-account",
    PASSWORDLESSLOGIN: APIURL + "auth/user/password-less/login",
    RESENDOTP: APIURL + "auth/user/resend-otp",
    RESETPWD: APIURL + "auth/user/reset-password",
    CHANGEPWD: APIURL + "auth/user/change-password",
    FORGOTPWD: APIURL + "auth/user/forgot-password",
    LOGOUT: APIURL + "auth/user/logout",
    HOMEPAGE: APIURL + "dashboard/home",

    PROFILE: APIURL + "auth/user/retrieve-profile",
    UPDATEPROFILE: APIURL + "auth/user/update-profile",
    UPDATEPROFILEPICTURE: APIURL + "auth/user/update-profile-pic",
    UPDATEPROFILEADDRESS: APIURL + "address/",
    MYLIKESDOCTOR: APIURL + "doctor/my-profile",
    MYLIKESCLINICS: APIURL + "doctor/clinics/my-profile",
    MYLIKESPHARMACIES: APIURL + "pharmacy/my-profile",
    MYLIKESSHOPS: APIURL + "shops/my-profile",
    MYLIKESFITNESS: APIURL + "fitness/my-profile",
    MYLIKESBLOGS: APIURL + "blogs/my-profile",
    MYLIKESVIDEOS: APIURL + "videos/my-profile",
    MYLIKESFLIPBOOKS: APIURL + "flip-books/my-profile",

    COUNTRIES: APIURL + "pharmacy/master/drop-down",

    LISTFILTERDATA: APIURL + "doctor/list-filters",

    CASCADEARTICLES: APIURL + "articles/cascade/",

    BLOGS: APIURL + "blogs/",
    BLOGBOOKMARK: APIURL + "blogs/bookmarks",
    BLOGLIKEDISLIKE: APIURL + "blogs/likes",
    BLOGREVIEWS: APIURL + "blogs/reviews",
    POPULARBLOGS: APIURL + "blogs/popular",

    VIDEOS: APIURL + "videos/",
    VIDEOBOOKMARK: APIURL + "videos/bookmarks",
    VIDEOLIKEDISLIKE: APIURL + "videos/likes",
    VIDEOSREVIEWS: APIURL + "videos/reviews",
    POPULARVIDEOS: APIURL + "videos/popular",


    FLIPBOOKS: APIURL + "flip-books/",
    FLIPBOOKBOOKMARK: APIURL + "flip-books/bookmarks",
    FLIPBOOKLIKEDISLIKE: APIURL + "flip-books/likes",
    FLIPBOOKSREVIEWS: APIURL + "flip-books/reviews",
    POPULARFLIPBOOKS: APIURL + "flip-books/popular",

    SHOPSDROPDOWNS: APIURL + "shops/list-filters",
    SHOPS: APIURL + "shops/",
    SHOPBOOKMARK: APIURL + "shops/bookmarks",
    SHOPLIKEDISLIKE: APIURL + "shops/likes",
    SHOPSREVIEWS: APIURL + "shops/reviews",
    RELATEDSHOPS: APIURL + "shops/related/",

    FITNESSDROPDOWNS: APIURL + "fitness/list-filters",
    FITNESS: APIURL + "fitness/",
    FITNESSBOOKMARK: APIURL + "fitness/bookmarks",
    FITNESSLIKEDISLIKE: APIURL + "fitness/likes",
    FITNESSSREVIEWS: APIURL + "fitness/reviews",
    RELATEDFITNESS: APIURL + "fitness/related/",

    PHARMACIESDROPDOWNS: APIURL + "pharmacy/list-filters",
    PHARMACIES: APIURL + "pharmacy/",
    PHARMACIESBOOKMARK: APIURL + "pharmacy/bookmarks",
    PHARMACIESLIKEDISLIKE: APIURL + "pharmacy/likes",
    PHARMACIESREVIEWS: APIURL + "pharmacy/reviews",
    RELATEDPHARMACY: APIURL + "pharmacy/related",

    DOCTORSDROPDOWNS: APIURL + "doctor/list-filters",
    DOCTORS: APIURL + "doctor/",
    DOCTORSBOOKMARK: APIURL + "doctor/bookmarks",
    DOCTORSLIKEDISLIKE: APIURL + "doctor/likes",
    DOCTORSREVIEWS: APIURL + "doctor/reviews",
    RELATEDDOCTOR: APIURL + "doctor/related/",

    CLINICSDROPDOWNS: APIURL + "doctor/clinics/list-filters",
    CLINICS: APIURL + "doctor/clinics/",
    CLINICSBOOKMARK: APIURL + "doctor/clinics/bookmarks",
    CLINICSLIKEDISLIKE: APIURL + "doctor/clinics/likes",
    CLINICSREVIEWS: APIURL + "doctor/clinics/reviews",
    RELATEDCLINIC: APIURL + "doctor/clinics/related/",

    FORUMSCATEGORY: APIURL + "forums/category",
    QUESTIONS: APIURL + "forums/question",
    CREATECOMMENTS: APIURL + "forums/create-comment",
    RETRIEVEFORUMS: APIURL + "forums/retrieve-question/",

    NEWSLETTER: APIURL + "dashboard/subscribe-newsletter"

  };
  authData: any;
  rootData: any = {};
  currentPage;
  isFooter = false;
  constructor(
    public http: Http,
    public route: ActivatedRoute,
    public router: Router,
    public toasterService: ToasterService,
    public datePipe: DatePipe,
    private titleService: Title
  ) {
    this.authData = this.isJson(localStorage.getItem("hnf_web"))
      ? JSON.parse(localStorage.getItem("hnf_web"))
      : undefined;
    this.init();
  }

  setTitle(title: string) {
    this.titleService.setTitle(title);
  }

  init() {
    this.router.events.subscribe(event => {
      let enterCnt = 0;
      // event instanceof NavigationStart || 
      if (event instanceof NavigationEnd) {
        console.log(enterCnt);
        if (enterCnt == 0) {
          console.log(this.currentPage);
          this.currentPage = event.url.split("/")[1]
            ? event.url.split("/")[1].split("?")[0]
            : "";
          setTimeout(() => {
            // $('.awemenu-active').removeClass('awemenu-active');
            if (document.querySelector(".awemenu-active")) {
              document
                .querySelector(".awemenu-active")
                .classList.remove("awemenu-active");
            }
          }, 500);
          setTimeout(() => {
            this.smoothTop();
          }, 400);

          setTimeout(() => {
            this.initJSModules();
          }, 500);

        }
        enterCnt++;
      }
    });
  }

  initJSModules() {
    $(window).on("scroll", function (a) {
      if ($(this).scrollTop() > 150) {
        $(".to-top").fadeIn(500);
      } else {
        $(".to-top").fadeOut(500)
      }
    });
    //   scroll to------------------
    $(".custom-scroll-link").on("click", function () {
      var a = 150 + $(".scroll-nav-wrapper").height();
      if (location.pathname.replace(/^\//, "") === this.pathname.replace(/^\//, "") || location.hostname === this.hostname) {
        var b = $(this.hash);
        b = b.length ? b : $("[name=" + this.hash.slice(1) + "]");
        if (b.length) {
          $("html,body").animate({
            scrollTop: b.offset().top - a
          }, {
            queue: false,
            duration: 1200,
            easing: "easeInOutExpo"
          });
          return false;
        }
      }
    });
    $(".to-top").on("click", function (a) {
      a.preventDefault();
      $("html, body").animate({
        scrollTop: 0
      }, 800);
      return false;
    });
    var modal: any = {};
    modal.hide = function () {
      $('.modal , .reg-overlay').fadeOut(200);
      $("html, body").removeClass("hid-body");
    };
    $('.modal-open').on("click", function (e) {
      e.preventDefault();
      $('.modal , .reg-overlay').fadeIn(200);
      $("html, body").addClass("hid-body");
    });
    $('.close-reg , .reg-overlay').on("click", function () {
      modal.hide();
    });
  }

  extractNum(param: string) {
    return parseInt(param.replace(/[^0-9\.]/g, ""), 10);
  }

  generateUUID() {
    let uuid = UUID.UUID();
    return uuid;
  }

  getURLId(key) {
    this.route.params.subscribe(params => {
      var result = key ? params[key] : params;
      return result;
    });
  }

  showErrorToast(message?, err?) {
    try {
      if (err) {
        if (err.non_field_errors) {
          if (Array.isArray(err.non_field_errors)) {
            message = err.non_field_errors[0];
          } else {
            message = err.non_field_errors;
          }
        } else {
          message = err[Object.keys(err)[0]];
        }
      }
    } catch (error) {
      message = "";
    }
    this.toast.error(
      message ? message : "oops, there is something went wrong."
    );
  }

  showSuccessToast(message?) {
    this.toast.success(message ? message : "");
  }

  logout() {
    this.postService(this.config.LOGOUT, {}, this.authData.auth_token)
      .then(res => {
        localStorage.clear();
        this.authData = undefined;
        this.toast.success("You have been logged out successfully.");
        this.go("/");
        // location.href = "/";
      })
      .catch(err => {
        this.toast.error("oops, there is something went wrong");
      });
  }

  smoothTop(timing = 800) {
    $("html, body").animate({ scrollTop: 0 }, timing);
  }

  smoothMove(id, timing = 800) {
    $("html, body").animate({ scrollTop: ($(id).offset().top - 100) }, timing);
  }

  loaderShow(time = 300) {
    $(".loader-wrap").fadeIn(0);
  }

  loaderHide(time = 700) {
    setTimeout(() => {
      $(".loader-wrap").fadeOut(300, function () {
        $("#main").animate({
          opacity: "1"
        }, 600);
      });
    }, time);
  }

  dateFormat(date, format = "yyyy-MM-dd") {
    return format
      ? this.datePipe.transform(date, format)
      : this.datePipe.transform(date);
  }

  go(name, param?, id?) {
    if (name == "/") {
      if (param) {
        this.router.navigate([""], {
          queryParams: { params: JSON.stringify(param) }
        });
      } else {
        this.router.navigate([""]);
      }
    } else {
      if (param) {
        if (id) {
          if (Object.keys(param).length > 0) {
            this.router.navigate([name, id], {
              queryParams: { params: JSON.stringify(param) },
              skipLocationChange: true
            });
          } else {
            this.router.navigate([name, id]);
          }
        } else {
          this.router.navigate([name], {
            queryParams: { params: JSON.stringify(param) }
          });
        }
      } else {
        this.router.navigate([name]);
      }
    }
  }

  getParams() {
    var url = location.href;
    var urlArr = url.split("?params=");
    return urlArr[urlArr.length - 1];
  }

  clone(param) {
    const clone: any = {};
    const keys = Object.keys(param);
    for (let i = 0; i < keys.length; i++) {
      clone[keys[i]] = param[keys[i]];
    }
    return clone;
  }

  triggerEvents(event: string, args: any) {
    var elem: any;
    if (args.id) {
      elem = document.getElementById(args.id)[0];
    } else if (args.class) {
      elem = document.getElementsByClassName(args.class)[0];
    }

    if (elem) {
      if (event == "click") {
        elem.click();
      }
    }
  }

  getUrlParams(key?) {
    var result = this.route.queryParams["value"]["params"];
    if (this.isJson(result)) {
      if (key) {
        return JSON.parse(result)[key];
      } else {
        return JSON.parse(result);
      }
    } else {
      return result;
    }
  }

  getPageName(param?) {
    if (param == 0) {
      this.isFooter = true;
    }
    return this.router.url.split("/")[1];
  }

  getObject(key: string) {
    var param = sessionStorage.getItem(key);
    return this.isJson(param) ? JSON.parse(param) : undefined;
  }

  isJson(param: string) {
    try {
      JSON.parse(param);
    } catch (err) {
      return false;
    }
    return true;
  }

  toast = {
    error: (message: String) => {
      var toast: Toast = {
        type: "error",
        // title: 'Title text',
        body: message,
        showCloseButton: false
      };
      if (document.querySelectorAll(".toast").length > 0) {
        document.querySelector(".toast").remove();
      }
      this.toasterService.pop(toast);
    },
    success: (message: String) => {
      var toast: Toast = {
        type: "success",
        body: message,
        showCloseButton: false
      };
      if (document.querySelectorAll(".toast").length > 0) {
        document.querySelector(".toast").remove();
      }
      this.toasterService.pop(toast);
    },
    info: (message: String) => {
      var toast: Toast = {
        type: "info",
        body: message,
        showCloseButton: false
      };
      if (document.querySelectorAll(".toast").length > 0) {
        document.querySelector(".toast").remove();
      }
      this.toasterService.pop(toast);
    }
  };

  forEach(options, elementCallback) {
    for (var option in options) {
      if (options.hasOwnProperty(option)) {
        elementCallback(options[option], option);
      }
    }
  }
  postService(url: string, options: object, token?: string) {
    return new Promise((resolve, reject) => {
      this.headers = new Headers();
      this.headers.append("Content-Type", "application/json");
      if (this.authData) {
        this.headers.append("Authorization", "Token " + this.authData.auth_token);
      }
      let reqOptions = new RequestOptions({ headers: this.headers });
      this.http.post(url, JSON.stringify(options), reqOptions).subscribe(
        (res: any) => {
          resolve(res._body ? res.json() : res);
        },
        (err: any) => {
          if (err.status == 406) {
            let result: any = {};
            result.status = err.status;
            if (err._body) {
              result.message = err.json();
            } else {
              result.message = err;
            }
            reject(result);
          } else {
            reject(err._body ? err.json() : err);
          }
        }
      );
    });
  }

  putService(url: string, options: object, token?: string) {
    return new Promise((resolve, reject) => {
      this.headers = new Headers();
      this.headers.append("Content-Type", "application/json");
      if (this.authData) {
        this.headers.append("Authorization", "Token " + this.authData.auth_token);
      }
      let reqOptions = new RequestOptions({ headers: this.headers });
      this.http.put(url, JSON.stringify(options), reqOptions).subscribe(
        (res: any) => {
          resolve(res._body ? res.json() : res);
        },
        (err: any) => {
          if (err.status == 422) {
            let result: any = {};
            result.status = err.status;
            if (err._body) {
              result.message = err.json();
            } else {
              result.message = err;
            }
            reject(result);
          } else {
            reject(err._body ? err.json() : err);
          }
        }
      );
    });
  }

  patchService(url: string, options: object, token?: string) {
    return new Promise((resolve, reject) => {
      this.headers = new Headers();
      this.headers.append("Content-Type", "application/json");
      if (this.authData) {
        this.headers.append("Authorization", "Token " + this.authData.auth_token);
      }
      let reqOptions = new RequestOptions({ headers: this.headers });
      this.http.patch(url, JSON.stringify(options), reqOptions).subscribe(
        (res: any) => {
          resolve(res._body ? res.json() : res);
        },
        (err: any) => {
          if (err.status == 422) {
            let result: any = {};
            result.status = err.status;
            if (err._body) {
              result.message = err.json();
            } else {
              result.message = err;
            }
            reject(result);
          } else {
            reject(err._body ? err.json() : err);
          }
        }
      );
    });
  }

  patchFormService(url: string, options: object, token?: string) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Accept", "application/json");
      if (this.authData) {
        headers.append("Authorization", "Token " + this.authData.auth_token);
      }
      let reqOptions = new RequestOptions({ headers: headers });
      this.http.patch(url, options, reqOptions).subscribe(
        (res: any) => {
          resolve(res._body ? res.json() : res);
        },
        (err: any) => {
          if (err.status == 422) {
            let result: any = {};
            result.status = err.status;
            if (err._body) {
              result.message = err.json();
            } else {
              result.message = err;
            }
            reject(result);
          } else {
            reject(err._body ? err.json() : err);
          }
        }
      );
    });
  }

  postFormService(url: string, options: object, token?: string) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Accept", "application/json");
      if (this.authData) {
        headers.append("Authorization", "Token " + this.authData.auth_token);
      }
      let reqOptions = new RequestOptions({ headers: headers });
      this.http.post(url, options, reqOptions).subscribe(
        (res: any) => {
          resolve(res._body ? res.json() : res);
        },
        (err: any) => {
          if (err.status == 422) {
            let result: any = {};
            result.status = err.status;
            if (err._body) {
              result.message = err.json();
            } else {
              result.message = err;
            }
            reject(result);
          } else {
            reject(err._body ? err.json() : err);
          }
        }
      );
    });
  }

  putFormService(url: string, options: object, token?: string) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "multipart/form-data");
      if (this.authData) {
        headers.append("Authorization", "Token " + this.authData.auth_token);
      }
      let reqOptions = new RequestOptions({ headers: headers });
      this.http.put(url, options, reqOptions).subscribe(
        (res: any) => {
          resolve(
            res._body ? (this.isJson(res.json()) ? res.json() : res) : res
          );
        },
        (err: any) => {
          if (err.status == 422) {
            let result: any = {};
            result.status = err.status;
            if (err._body) {
              result.message = err.json();
            } else {
              result.message = err;
            }
            reject(result);
          } else {
            reject(err._body ? err.json() : err);
          }
        }
      );
    });
  }

  postServiceWithOptions(url: string, options: object) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("Accept-Language", "en");
      headers.append("crossDomain", "true");
      let reqOptions = new RequestOptions();
      reqOptions.withCredentials = true;
      reqOptions.headers = headers;
      this.http.post(url, JSON.stringify(options), reqOptions).subscribe(
        (res: any) => {
          resolve(res._body ? res.json() : res);
        },
        err => {
          reject("Authentication Failed. Please try again later");
        }
      );
    });
  }

  getService(url: string, token?: string) {
    return new Promise((resolve, reject) => {
      this.headers = new Headers({ "Content-Type": "application/json" });
      if (this.authData) {
        this.headers.append("Authorization", "Token " + this.authData.auth_token);
      }
      let reqOptions = new RequestOptions({ headers: this.headers });
      this.http.get(url, reqOptions).subscribe(
        (res: any) => {
          resolve(res._body ? res.json() : res);
        },
        (err: any) => {
          if (err.status == 404) {
            reject({ status: err.status, message: "Page Not Found" });
          } else {
            if (err.status == 401) {
              sessionStorage.clear();
              sessionStorage.clear();
              this.go("/");
            }
            reject(err._body ? err.json() : err);
          }
        }
      );
    });
  }

  deleteService(url: string, options?: object, token?: string) {
    return new Promise((resolve, reject) => {
      this.headers = new Headers({ "Content-Type": "application/json" });
      if (this.authData) {
        this.headers.append("Authorization", "Token " + this.authData.auth_token);
      }
      let reqOptions = new RequestOptions({
        headers: this.headers,
        body: JSON.stringify(options)
      });
      this.http.delete(url, reqOptions).subscribe(
        (res: any) => {
          resolve(res._body ? res.json() : res);
        },
        (err: any) => {
          if (err.status == 422) {
            let result: any = {};
            result.status = err.status;
            if (err._body) {
              result.message = err.json();
            } else {
              result.message = err;
            }
            reject(result);
          } else {
            reject(err._body ? err.json() : err);
          }
        }
      );
    });
  }

  replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, "g"), replace);
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  dateCompare(date1, date2) {
    if (date1 && date2) {
      var dt1, dt2;
      dt1 = typeof date1 == "string" ? new Date(date1) : date1;
      dt2 = typeof date2 == "string" ? new Date(date2) : date2;
      if (dt1.getTime() == dt2.getTime()) {
        return 0;
      } else if (dt1.getTime() > dt2.getTime()) {
        return 1;
      } else {
        return -1;
      }
    } else {
      return null;
    }
  }

  jsonToFormdata(param: any) {
    let formdata = new FormData(),
      value,
      isString = true;
    for (let key in param) {
      value = param[key];
      if (typeof value == "string" || typeof value == "object") {
        if (typeof value == "object" && !Array.isArray(value)) {
          if (value && value.type && value.type.indexOf("image/") >= 0) {
            isString = true;
          } else {
            isString = false;
          }
        } else {
          isString = false;
        }
      } else {
        isString = false;
      }
      if (value) {
        formdata.append(
          key,
          isString
            ? value
            : typeof value == "string"
              ? value
              : JSON.stringify(value)
        );
      }
    }
    return formdata;
  }

  initMapClusters(clusters: Array<Locations>, currLocation: any = {}) {
    var markerIcon = {
      anchor: new google.maps.Point(12, 0),
      url: 'assets/images/marker.png',
      labelOrigin: new google.maps.Point(25, 20)
    }
    function mainMap() {
      function locationData(locationURL, locationImg, locationTitle, locationAddress, locationStarRating, locationName) {
        return ('<div class="map-popup-wrap"><div class="map-popup"><div class="infoBox-close"><i class="far fa-times"></i></div><a href="' + locationURL + '" class="listing-img-content fl-wrap"><img src="' + locationImg + '" alt=""><span class="map-popup-location-price"><strong>' + locationName + '</strong></span></a> <div class="listing-content fl-wrap"><div class="card-popup-raining map-card-rainting" data-staRrating="' + locationStarRating + '"></div><div class="listing-title fl-wrap"><h4><a href=' + locationURL + '>' + locationTitle + '</a></h4><span class="map-popup-location-info"><i class="fas fa-map-marker-alt"></i>' + locationAddress + '</span></div></div></div></div>')
      }
      var locations = [];
      clusters.forEach((location: any) => {
        locations.push([
          locationData(location.navigateTo, location.image, location.title, location.address, location.review_count, location.name)
          , location.lat, location.lng, location.index, markerIcon]);
      });
      //   Map Infoboxes end ------------------
      var map = new google.maps.Map(document.getElementById('map-main'), {
        zoom: 10,
        scrollwheel: false,
        center: new google.maps.LatLng(currLocation.latitude || 40.7, currLocation.longitude || -73.87),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        panControl: false,
        fullscreenControl: true,
        navigationControl: false,
        streetViewControl: false,
        animation: google.maps.Animation.BOUNCE,
        gestureHandling: 'cooperative',
        styles: [{
          "featureType": "poi.attraction",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.business",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.medical",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.place_of_worship",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.school",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "transit.station.bus",
          "stylers": [{
            "visibility": "off"
          }]

        }
        ]
      });
      var boxText: any = document.createElement("div");
      boxText.className = 'map-box'
      var currentInfobox;
      var boxOptions = {
        content: boxText,
        disableAutoPan: true,
        alignBottom: true,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(-137, -25),
        zIndex: null,
        boxStyle: {
          width: "260px"
        },
        closeBoxMargin: "0",
        closeBoxURL: "",
        infoBoxClearance: new google.maps.Size(1, 1),
        isHidden: false,
        pane: "floatPane",
        enableEventPropagation: false,
      };

      var markerCluster, marker, i;
      var allMarkers = [];
      var clusterStyles = [{
        textColor: 'white',
        url: '',
        height: 50,
        width: 50
      }];
      for (i = 0; i < locations.length; i++) {
        var labels = '123456789';
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          icon: locations[i][4],
          id: i,
          label: {
            text: "" + (i + 1),
            color: "#3AACED",
            fontSize: "11px",
            fontWeight: "bold",
          },
        });
        allMarkers.push(marker);
        var ib: any = new InfoBox();
        google.maps.event.addListener(ib, "domready", function () {
          cardRaining()
        });
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
          return function () {
            ib.setOptions(boxOptions);
            boxText.innerHTML = locations[i][0];
            ib.close();
            ib.open(map, marker);
            currentInfobox = marker.id;
            var latLng = new google.maps.LatLng(locations[i][1], locations[i][2]);
            map.panTo(latLng);
            map.panBy(0, -50);
            google.maps.event.addListener(ib, 'domready', function () {
              $('.infoBox-close').click(function (e) {
                e.preventDefault();
                ib.close();
              });
            });
          }
        })(marker, i));
      }
      var options2 = {
        imagePath: 'assets/images/',
        styles: clusterStyles,
        minClusterSize: 2
      };
      markerCluster = new MarkerClusterer(map, allMarkers, options2);
      google.maps.event.addDomListener(window, "resize", function () {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
      });
      if ($(".controls-mapwn").length) {
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.addListener('bounds_changed', function () {
          searchBox.setBounds(map.getBounds());
        });
        var markers = [];
        searchBox.addListener('places_changed', function () {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }
          markers.forEach(function (marker) {
            marker.setMap(null);
          });
          markers = [];

          var bounds = new google.maps.LatLngBounds();
          places.forEach(function (place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }
      $('.map-item').on("click", function (e) {
        e.preventDefault();
        map.setZoom(15);
        var index = currentInfobox;
        var marker_index = parseInt($(this).attr('href').split('#')[1], 10);
        google.maps.event.trigger(allMarkers[marker_index], "click");
        if ($(window).width() > 1064) {
          if ($(".map-container").hasClass("fw-map")) {
            $('html, body').animate({
              scrollTop: $(".map-container").offset().top + "-110px"
            }, 1000)
            return false;
          }
        }
      });
      $('.nextmap-nav').on("click", function (e) {
        e.preventDefault();
        map.setZoom(15);
        var index = currentInfobox;
        if (index + 1 < allMarkers.length) {
          google.maps.event.trigger(allMarkers[index + 1], 'click');
        } else {
          google.maps.event.trigger(allMarkers[0], 'click');
        }
      });
      $('.prevmap-nav').on("click", function (e) {
        e.preventDefault();
        map.setZoom(15);
        if (typeof (currentInfobox) == "undefined") {
          google.maps.event.trigger(allMarkers[allMarkers.length - 1], 'click');
        } else {
          var index = currentInfobox;
          if (index - 1 < 0) {
            google.maps.event.trigger(allMarkers[allMarkers.length - 1], 'click');
          } else {
            google.maps.event.trigger(allMarkers[index - 1], 'click');
          }
        }
      });
      var zoomControlDiv: any = document.createElement('div');
      var zoomControl: any = new ZoomControl(zoomControlDiv, map);
      function ZoomControl(controlDiv, map) {
        zoomControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);
        controlDiv.style.padding = '5px';
        var controlWrapper = document.createElement('div');
        controlDiv.appendChild(controlWrapper);
        var zoomInButton = document.createElement('div');
        zoomInButton.className = "mapzoom-in";
        controlWrapper.appendChild(zoomInButton);
        var zoomOutButton = document.createElement('div');
        zoomOutButton.className = "mapzoom-out";
        controlWrapper.appendChild(zoomOutButton);
        google.maps.event.addDomListener(zoomInButton, 'click', function () {
          map.setZoom(map.getZoom() + 1);
        });
        google.maps.event.addDomListener(zoomOutButton, 'click', function () {
          map.setZoom(map.getZoom() - 1);
        });
      }
    }
    var map = document.getElementById('map-main');
    if (typeof (map) != 'undefined' && map != null) {
      // google.maps.event.addDomListener(window, 'load', mainMap);
      mainMap();
    }
  }

  retrieveMyLocation() {
    return new Promise((resolve) => {
      navigator.geolocation.getCurrentPosition((coords: any) => {
        console.log(coords);
        resolve(coords)
      }, err => {
        console.log(err);
        resolve(err);
      })
    });
  }

  openMap(lat, lng) {
    let coords = lat + ',' + lng;
    window.open('https:// https://www.google.com/maps/search/' + coords, '_blank');
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  googleSignIn() {
    return GoogleService.signIn(gClientid)
    // .then(res => {
    //   console.log(res);
    //   GoogleService.getLoginStatus().then(res => console.log(res)).catch(err => console.log(err));
    // }).catch(err => {
    //   console.log(err);
    // })
  }

  facebookSignIn() {
    return FacebookService.signIn(fAppId);
  }

}
