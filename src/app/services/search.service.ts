import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { GlobalService } from './global.service';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  queryUrl: string = '';
  constructor(private http: Http, private common: GlobalService) { }

  search(url: string, terms: Observable<string>, queryUrl = "?search=") {
    if (queryUrl != "?search=") {
      if(queryUrl.indexOf("?")>=0){
        this.queryUrl = queryUrl;
      }else{
        this.queryUrl = "?" + queryUrl + "=";
      }
    } else {
      this.queryUrl = queryUrl;
    }
    return terms.debounceTime(500)
      .distinctUntilChanged()
      .switchMap(term => this.searchEntries(url, term));
  }

  searchEntries(url, term) {
    return this.common.getService(url + this.queryUrl + term);
  }
}
