import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'filter'
})
export class FilterPipe implements PipeTransform {
    transform(items: any[], searchText: string, key: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toLowerCase();
        return items.filter(it => {
            if (key) {
                return it[key].toLowerCase().indexOf(searchText) === -1 ? false : true;
            } else {
                return it.toLowerCase().indexOf(searchText) === -1 ? false : true;
            }
        });
    }
}