import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services';
declare var $;
interface Locations {
  navigateTo: string;
  name: string;
  image: string;
  address: string;
  title: string;
  reviews_count: number;
  index: number;
  lat: string;
  lng: string;
}

@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.scss']
})
export class DoctorsComponent implements OnInit {

  categoriesList: any = {};
  searchText = "";
  shopsList: any = {};
  locationsList: Array<Locations> = [];
  currLocation: any = {};
  currCategory = "";
  params = "";
  isComplete = false;
  constructor(public common: GlobalService) {
    this.common.loaderShow();
  }

  ngOnInit() {
    this.common.retrieveMyLocation().then((coords: any) => {
      console.log(coords.coords);
      if (coords) {
        this.currLocation = {
          latitude: coords.coords.latitude,
          longitude: coords.coords.longitude,
          city: "",
        };
      }
      console.log(this.currLocation);
      // this.listShops();
    }).catch(err => {
      console.log(err);
      // this.listShops();
    });
    this.listShops();
    this.listDropDowns();
    this.initJS();
  }

  initJS() {
    setTimeout(() => {
      $(".list-single-facts .inline-facts-wrap").matchHeight({});
      $(".listing-item-container .listing-item").matchHeight({});
      $(".article-masonry").matchHeight({});
      $('.category-select').niceSelect();
      $('.city-select').niceSelect();
      $('.category-select').on('change', (e) => {
        console.log(e.target.value);
      })

      $('.geodir-category-content-title:before').on('click', (e) => {
        console.log(e);
      })
    }, 500);
  }

  filterShops() {
    let params = "";
    params += "?q=" + this.searchText;
    params += "&about__qualification=" + $('.category-select.quali').val();
    params += "&specialization__specialization=" + $('.category-select.spec').val();
    this.currCategory = $('.category-select.quali :selected').text() == "All Qualifications" ? "" : $('.category-select.quali :selected').text();
    if ($('.category-select.spec :selected').text() && $('.category-select.spec :selected').text() != "All Specializations") {
      this.currCategory = this.currCategory ? (this.currCategory + ', ' + $('.category-select.spec :selected').text()) : $('.category-select.spec :selected').text();
    }
    this.params = params;
    this.listShops(params);
  }

  listDropDowns() {
    this.common.getService(this.common.config.DOCTORSDROPDOWNS).then((res: any) => {
      console.log(res);
      this.categoriesList = res;
    }).catch(err => { });
  }
  listShops(params = "", isMore = false) {
    $('.load-more-button').append(`<i class="fal fa-spinner"></i>`);
    let url = this.common.config.DOCTORS;
    if (isMore && this.shopsList.next) {
      url = this.shopsList.next
    }
    this.common.getService(url + params).then((res: any) => {
      console.log(res);
      if (!res.next) {
        this.isComplete = true;
      }else{
        this.isComplete = false;
      }
      if (isMore) {
        let temp = {
          count: res.count,
          next: res.next,
          previous: res.previous,
          results: this.shopsList.results.concat(res.results)
        };
        this.shopsList = temp;
      } else {
        this.shopsList = res;
      }
      this.locationsList = [];
      this.shopsList.results.forEach((newitem, key) => {
        let item = newitem.clinic_info[0];
        if (item) {
          this.locationsList.push({
            navigateTo: 'doctor-detail/' + item.id,
            name: item.name,
            image: item.banner_image,
            address: '',
            title: item.name,
            reviews_count: item.reviews_count,
            index: key,
            lat: item.address.latitude,
            lng: item.address.longitude
          });
        }
      });
      console.log(this.locationsList);
      setTimeout(() => {
        this.common.initMapClusters(this.locationsList, this.currLocation);
      }, 500);
      this.common.loaderHide();
    }).catch(err => {
      $('.load-more-button i').remove();
      this.common.loaderHide();
    });
  }

  loadmore() {
    this.listShops(this.params, true);
  }

}
