import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services';
import { ActivatedRoute } from '@angular/router';

import * as Plyr from 'plyr';

@Component({
  selector: 'app-video-detail',
  templateUrl: './video-detail.component.html',
  styleUrls: ['./video-detail.component.scss']
})
export class VideoDetailComponent implements OnInit {
  videoId = "";
  videoDetail: any = {};
  reviewList: any = {};
  myreview = '';
  constructor(public common: GlobalService, public route: ActivatedRoute) {
    this.common.loaderShow();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.videoId = params.id
      console.log(this.videoId);
      this.retrieveVideo();
      this.listReviews();
    });
  }

  retrieveVideo() {
    this.common.getService(this.common.config.VIDEOS + this.videoId + "/").then(res => {
      this.videoDetail = res;
      this.common.loaderHide();
      setTimeout(() => {
        const player = new Plyr('video', { captions: { active: true } });
        window['player'] = player;
      }, 500);
    }).catch(err => {
      this.common.loaderHide();
    });
  }

  toggleBookmark() {
    this.common.postService(this.common.config.VIDEOBOOKMARK, { video: this.videoDetail.id }).then(res => {
      this.common.showSuccessToast("Bookmark added");
      this.retrieveVideo();
    }).catch(err => {
      console.log(err);
    });
  }

  toggleLike() {
    this.common.postService(this.common.config.VIDEOLIKEDISLIKE, { video: this.videoDetail.id }).then(res => {
      this.retrieveVideo();
    }).catch(err => {
      console.log(err);
    });
  }

  goAddReview() {
    this.common.smoothMove("#sec6");
  }

  listReviews() {
    this.common.getService(this.common.config.VIDEOSREVIEWS + "?blog=" + this.videoId).then(res => {
      console.log(res);
      this.reviewList = res;
    }).catch(err => {
      console.log(err);
    })
  }

  submitReview() {
    if (this.myreview) {
      let query = {
        "video": this.videoId,
        "text": this.myreview
      };
      this.common.postService(this.common.config.VIDEOSREVIEWS, query).then(res => {
        this.common.showSuccessToast("Your review has been posted.");
        this.retrieveVideo();
        this.listReviews();
      }).catch(err => {
        console.log(err);
      });
    }
  }


}
