import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { GlobalService, FilterPipe } from './services';
import { HttpModule } from '@angular/http';
import { ToasterModule } from 'angular2-toaster';
import { DatePipe } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { BlogsComponent } from './blogs/blogs.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { VideosComponent } from './videos/videos.component';
import { FlipbooksComponent } from './flipbooks/flipbooks.component';
import { VideoDetailComponent } from './video-detail/video-detail.component';
import { FlipbookDetailComponent } from './flipbook-detail/flipbook-detail.component';
import { ShopsComponent } from './shops/shops.component';
import { ShopDetailComponent } from './shop-detail/shop-detail.component';
import { FitnessComponent } from './fitness/fitness.component';
import { FitnessDetailComponent } from './fitness-detail/fitness-detail.component';
import { PharmaciesDetailComponent } from './pharmacies-detail/pharmacies-detail.component';
import { PharmaciesComponent } from './pharmacies/pharmacies.component';
import { DoctorsComponent } from './doctors/doctors.component';
import { ClinicsComponent } from './clinics/clinics.component';
import { DoctorDetailComponent } from './doctor-detail/doctor-detail.component';
import { ClinicDetailComponent } from './clinic-detail/clinic-detail.component';
import { ForumsComponent } from './forums/forums.component';
import { ForumDetailComponent } from './forum-detail/forum-detail.component';
import { NumberDirective } from './services/numbers-only.directive';
import { ViewFlipbookComponent } from './view-flipbook/view-flipbook.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProfileComponent,
    FilterPipe,
    BlogsComponent,
    BlogDetailComponent,
    VideosComponent,
    FlipbooksComponent,
    VideoDetailComponent,
    FlipbookDetailComponent,
    ShopsComponent,
    ShopDetailComponent,
    FitnessComponent,
    FitnessDetailComponent,
    PharmaciesDetailComponent,
    PharmaciesComponent,
    DoctorsComponent,
    ClinicsComponent,
    DoctorDetailComponent,
    ClinicDetailComponent,
    ForumsComponent,
    ForumDetailComponent,
    NumberDirective,
    ViewFlipbookComponent,
    PrivacyPolicyComponent,
    TermsConditionsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    ToasterModule.forRoot(),
    BrowserAnimationsModule,
  ],
  providers: [
    GlobalService,
    DatePipe,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
