import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.scss']
})
export class BlogDetailComponent implements OnInit {
  blogId: any = "";
  blogDetail: any = {};
  myreview = "";
  reviewList: any = {};
  constructor(public common: GlobalService, public route: ActivatedRoute) {
    this.common.loaderShow();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.blogId = params.id
      console.log(this.blogId);
      this.retrieveBlog();
      this.listReviews();
    });
  }

  retrieveBlog() {
    this.common.getService(this.common.config.BLOGS + this.blogId).then(res => {
      this.blogDetail = res;
      this.common.loaderHide();
    }).catch(err => {
      this.common.loaderHide();
    })
  }

  toggleBookmark() {
    this.common.postService(this.common.config.BLOGBOOKMARK, { blog: this.blogDetail.id }).then(res => {
      this.common.showSuccessToast("Bookmark added");
      this.retrieveBlog();
    }).catch(err => {
      console.log(err);
    });
  }

  toggleLike() {
    this.common.postService(this.common.config.BLOGLIKEDISLIKE, { blog: this.blogDetail.id }).then(res => {
      this.retrieveBlog();
    }).catch(err => {
      console.log(err);
    });
  }

  goAddReview() {
    this.common.smoothMove("#sec6");
  }

  listReviews() {
    this.common.getService(this.common.config.BLOGREVIEWS + "?blog=" + this.blogId).then(res => {
      console.log(res);
      this.reviewList = res;
    }).catch(err => {
      console.log(err);
    })
  }

  submitReview() {
    if (this.myreview) {
      let query = {
        "blog": this.blogId,
        "text": this.myreview
      };
      this.common.postService(this.common.config.BLOGREVIEWS, query).then(res => {
        this.common.showSuccessToast("Your review has been posted.");
        this.retrieveBlog();
        this.listReviews();
      }).catch(err => {
        console.log(err);
      });
    }
  }

}
