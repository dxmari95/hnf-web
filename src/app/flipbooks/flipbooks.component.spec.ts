import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlipbooksComponent } from './flipbooks.component';

describe('FlipbooksComponent', () => {
  let component: FlipbooksComponent;
  let fixture: ComponentFixture<FlipbooksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlipbooksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlipbooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
