import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { GlobalService, SearchService } from '../services';

@Component({
  selector: 'app-flipbooks',
  templateUrl: './flipbooks.component.html',
  styleUrls: ['./flipbooks.component.scss']
})
export class FlipbooksComponent implements OnInit {

  flipbookList = {
    next: null,
    prev: null,
    count: 0,
    results: []
  };
  searchText = "";
  searchTerm$ = new Subject<string>();
  cascadeList = [];
  currCategory: any = {};
  currSubCategory: any = {};
  params = "?q=";
  popularList = [];
  constructor(public common: GlobalService, public searchService: SearchService) {
    this.common.loaderShow();
  }

  ngOnInit() {
    this.searchService.search(this.common.config.FLIPBOOKS, this.searchTerm$,"?q=").subscribe((data: any) => {
      console.log(data);
      this.flipbookList = data;
    });
    this.listBlogs();
    this.listCascadeCategories();
    this.listPopularBooks();
  }

  listBlogs() {
    this.params = "?q=" + this.searchText;
    if(this.currCategory.id){
      this.params += "&subcategory__category=" + this.currCategory.id;
    }
    if(this.currSubCategory.id){
      this.params += "&subcategory=" + this.currSubCategory.id;
    }
    this.common.getService(this.common.config.FLIPBOOKS + this.params).then((res: any) => {
      console.log(res);
      this.flipbookList = res;
      this.common.loaderHide();
    }).catch(err => {
      this.common.loaderHide();
      console.log(err);
    })
  }

  listCascadeCategories() {
    this.common.getService(this.common.config.CASCADEARTICLES).then((res: any) => {
      console.log(res);
      this.cascadeList = res;
      this.currCategory = this.cascadeList[0];
      console.log(this.currCategory)
    }).catch(err => {
      console.log(err);
    })
  }

  selectCategory(cat) {
    this.currCategory = cat;
    this.listBlogs();
  }

  selectSubCategory(subcat) {
    this.currSubCategory = subcat;
    this.listBlogs();
  }

  listPopularBooks(){
    this.common.getService(this.common.config.POPULARFLIPBOOKS).then((res: any) => {
      this.popularList = res;
    }).catch(err => {
      console.log(err);
    })
  }

}
