import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services';
declare var $;
interface Locations {
  navigateTo: string;
  name: string;
  image: string;
  address: string;
  title: string;
  reviews_count: number;
  index: number;
  lat: string;
  lng: string;
}
@Component({
  selector: 'app-shops',
  templateUrl: './shops.component.html',
  styleUrls: ['./shops.component.scss']
})
export class ShopsComponent implements OnInit {
  categoriesList: any = {};
  searchText = "";
  shopsList: any = {};
  locationsList: Array<Locations> = [];
  currLocation: any = {};
  currCategory = "";
  constructor(public common: GlobalService) {
    this.common.loaderShow();
  }

  async ngOnInit() {
    await this.listShops();
    this.listDropDowns();
    this.initJS();
  }

  retreiveLocation() {
    return new Promise((resolve, reject) => {
      this.common.retrieveMyLocation().then((coords: any) => {
        console.log(coords.coords);
        if (coords) {
          this.currLocation = {
            latitude: coords.coords.latitude,
            longitude: coords.coords.longitude,
            city: "",
          };
        }
        console.log(this.currLocation);
        resolve();
      }).catch(err => {
        console.log(err);
        resolve();
      });
    });
  }

  initJS() {
    setTimeout(() => {
      $(".grid-opt li span").on("click", function () {
        $(".listing-item").matchHeight({
          remove: true
        });
        setTimeout(function () {
          $(".listing-item").matchHeight();
        }, 50);
        $(".grid-opt li span").removeClass("act-grid-opt");
        $(this).addClass("act-grid-opt");
        if ($(this).hasClass("two-col-grid")) {
          $(".listing-item").removeClass("has_one_column");
          $(".listing-item").addClass("has_two_column");
        } else if ($(this).hasClass("one-col-grid")) {
          $(".listing-item").addClass("has_one_column");
        } else {
          $(".listing-item").removeClass("has_one_column").removeClass("has_two_column");
        }
      });
      $('.category-select').niceSelect();
      $('.city-select').niceSelect();
      $('.category-select').on('change', (e) => {
        console.log(e.target.value);
      })
    }, 500);
  }

  filterShops() {
    let params = "";
    params += "?q=" + this.searchText;
    params += "&category=" + $('.category-select').val();
    this.currCategory = $('.category-select :selected').text();
    this.listShops(params);
  }

  listDropDowns() {
    this.common.getService(this.common.config.SHOPSDROPDOWNS).then((res: any) => {
      console.log(res);
      this.categoriesList = res;
    }).catch(err => { });
  }

  listShops(params = "") {
    this.common.getService(this.common.config.SHOPS + params).then((res: any) => {
      console.log(res);
      this.shopsList = res;
      this.locationsList = [];
      this.shopsList.results.forEach((item, key) => {
        this.locationsList.push({
          navigateTo: 'shop-detail/' + item.id,
          name: item.name,
          image: item.banner_image,
          address: item.address.address_line_1 + ", " + item.address.address_line_1 + ", " + item.address.city,
          title: item.name,
          reviews_count: item.reviews_count,
          index: key,
          lat: item.address.latitude,
          lng: item.address.longitude
        });
      });
      console.log(this.locationsList);
      setTimeout(() => {
        this.common.initMapClusters(this.locationsList, this.currLocation);
      }, 500);
      this.common.loaderHide();
    }).catch(err => {
      this.common.loaderHide();
    });
  }

}
