import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services';
import { ActivatedRoute } from '@angular/router';
declare var $;
@Component({
  selector: 'app-shop-detail',
  templateUrl: './shop-detail.component.html',
  styleUrls: ['./shop-detail.component.scss']
})
export class ShopDetailComponent implements OnInit {
  shopDetail: any = {};
  shopId = "";
  reviewList: any = {};
  myreview = '';
  otherPhotosList: any = "";
  relatedList = [];
  constructor(public common: GlobalService, public route: ActivatedRoute) {
    this.common.loaderShow();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.shopId = params.id
      console.log(this.shopId);
      this.listRelatedItems();
      this.retrieveShop().then(() => {
        this.initLightSlider();
      }).catch(err => { });
      this.listReviews();
    });
    this.initJS()
  }

  initJS() {
    setTimeout(() => {
      var a = $(".bg");
      a.each(function (a) {
        if ($(this).attr("data-bg")) $(this).css("background-image", "url(" + $(this).data("bg") + ")");
      });
      var self: any = window;
      self.initparallax();
    }, 500);
  }

  initLightSlider() {
    setTimeout(() => {
      function initIsotope() {
        if ($(".gallery-items").length) {
          var a = $(".gallery-items").isotope({
            singleMode: true,
            columnWidth: ".grid-sizer, .grid-sizer-second, .grid-sizer-three",
            itemSelector: ".gallery-item, .gallery-item-second, .gallery-item-three",
            transformsEnabled: true,
            transitionDuration: "700ms",
            resizable: true
          });
          a.imagesLoaded(function () {
            a.isotope("layout");
          });
        }
      }
      initIsotope();
      $(".image-popup").lightGallery({
        selector: "this",
        cssEasing: "cubic-bezier(0.25, 0, 0.25, 1)",
        download: false,
        counter: false
      });
      var o = $(".lightgallery"),
        p = o.data("looped");
      o.lightGallery({
        selector: ".lightgallery a.popup-image",
        cssEasing: "cubic-bezier(0.25, 0, 0.25, 1)",
        download: false,
        loop: false,
        counter: false
      });
      function initHiddenGal() {
        $(".dynamic-gal").on('click', function () {
          var dynamicgal = eval($(this).attr("data-dynamicPath"));

          $(this).lightGallery({
            dynamic: true,
            dynamicEl: dynamicgal,
            download: false,
            loop: false,
            counter: false
          });

        });
      }
      initHiddenGal();
    }, 200);
  }

  retrieveShop() {
    return new Promise((resolve, reject) => {
      this.common.getService(this.common.config.SHOPS + this.shopId + "/").then(res => {
        this.shopDetail = res;
        if (this.shopDetail.images.length > 5) {
          let temp = [];
          this.shopDetail.images.forEach((val, key) => {
            if (key > 5) {
              temp.push({
                "src": val.image
              });
            }
          })
          this.otherPhotosList = JSON.stringify(temp);
        }
        console.log(this.otherPhotosList);
        this.common.loaderHide();
        resolve();
      }).catch(err => {
        this.common.loaderHide();
        reject(err);
      });
    })
  }


  listRelatedItems() {
    this.common.getService(this.common.config.RELATEDSHOPS + this.shopId).then((res: any) => {
      this.relatedList = res;
    }).catch(err => {
      console.log(err);
    })
  }

  toggleBookmark() {
    this.common.postService(this.common.config.SHOPBOOKMARK, { shop: this.shopDetail.id }).then(res => {
      this.common.showSuccessToast("Bookmark added");
      this.retrieveShop();
    }).catch(err => {
      console.log(err);
    });
  }

  toggleLike() {
    this.common.postService(this.common.config.SHOPLIKEDISLIKE, { shop: this.shopDetail.id }).then(res => {
      this.retrieveShop();
    }).catch(err => {
      console.log(err);
    });
  }

  goAddReview() {
    this.common.smoothMove("#sec6");
  }

  listReviews() {
    this.common.getService(this.common.config.SHOPSREVIEWS + "?shop=" + this.shopId).then(res => {
      console.log(res);
      this.reviewList = res;
    }).catch(err => {
      console.log(err);
    })
  }

  submitReview() {
    if (this.myreview) {
      let query = {
        "shop": this.shopId,
        "text": this.myreview
      };
      this.common.postService(this.common.config.SHOPSREVIEWS, query).then(res => {
        this.common.showSuccessToast("Your review has been posted.");
        this.retrieveShop();
        this.listReviews();
        this.myreview = "";
      }).catch(err => {
        console.log(err);
      });
    }
  }
}
