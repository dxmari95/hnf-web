import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services';
declare var $;
interface Locations {
  navigateTo: string;
  name: string;
  image: string;
  address: string;
  title: string;
  reviews_count: number;
  index: number;
  lat: string;
  lng: string;
}

@Component({
  selector: 'app-clinics',
  templateUrl: './clinics.component.html',
  styleUrls: ['./clinics.component.scss']
})
export class ClinicsComponent implements OnInit {


  categoriesList: any = {};
  searchText = "";
  shopsList: any = {};
  locationsList: Array<Locations> = [];
  currLocation: any = {};
  currCategory = "";
  constructor(public common: GlobalService) {
    this.common.loaderShow();
  }

  async ngOnInit() {
    this.listDropDowns();
    await this.retreiveLocation();
    this.listShops();
    this.initJS();
  }

  retreiveLocation() {
    return new Promise((resolve, reject) => {
      this.common.retrieveMyLocation().then((coords: any) => {
        console.log(coords.coords);
        if (coords) {
          this.currLocation = {
            latitude: coords.coords.latitude,
            longitude: coords.coords.longitude,
            city: "",
          };
        }
        console.log(this.currLocation);
        resolve();
      }).catch(err => {
        console.log(err);
        resolve();
      });
    });
  }

  initJS() {
    setTimeout(() => {
      $(".list-single-facts .inline-facts-wrap").matchHeight({});
      $(".listing-item-container .listing-item").matchHeight({});
      $(".article-masonry").matchHeight({});
      $('.category-select').niceSelect();
      $('.city-select').niceSelect();
      $('.category-select').on('change', (e) => {
        console.log(e.target.value);
      })
    }, 500);
  }

  filterShops() {
    let params = "";
    params += "?q=" + this.searchText;
    params += "&category=" + $('.category-select').val();
    this.currCategory = $('.category-select :selected').text();
    this.listShops(params);
  }

  listDropDowns() {
    this.common.getService(this.common.config.CLINICSDROPDOWNS).then((res: any) => {
      console.log(res);
      this.categoriesList = res;
    }).catch(err => { });
  }

  listShops(params = "") {
    this.common.getService(this.common.config.CLINICS + params).then((res: any) => {
      console.log(res);
      this.shopsList = res;
      this.locationsList = [];
      this.shopsList.results.forEach((item, key) => {
        this.locationsList.push({
          navigateTo: 'clinic-detail/' + item.id,
          name: item.name,
          image: item.banner_image,
          address: item.address.address_line_1 + ", " + item.address.address_line_1 + ", " + item.address.city,
          title: item.name,
          reviews_count: item.reviews_count,
          index: key,
          lat: item.address.latitude,
          lng: item.address.longitude
        });
      });
      console.log(this.locationsList);
      setTimeout(() => {
        this.common.initMapClusters(this.locationsList, this.currLocation);
      }, 500);
      this.common.loaderHide();
    }).catch(err => {
      this.common.loaderHide();
    });
  }

}
