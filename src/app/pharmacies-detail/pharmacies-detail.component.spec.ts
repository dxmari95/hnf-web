import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PharmaciesDetailComponent } from './pharmacies-detail.component';

describe('PharmaciesDetailComponent', () => {
  let component: PharmaciesDetailComponent;
  let fixture: ComponentFixture<PharmaciesDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PharmaciesDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PharmaciesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
