import { Component, OnInit } from '@angular/core';
import { GlobalService, SearchService } from '../services';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.scss']
})
export class BlogsComponent implements OnInit {
  blogList = {
    next: null,
    prev: null,
    count: 0,
    results: []
  };
  searchText = "";
  searchTerm$ = new Subject<string>();
  cascadeList = [];
  currCategory: any = {};
  currSubCategory: any = {};
  params = "?q=";
  popularList = [];
  constructor(public common: GlobalService, public searchService: SearchService) {
    this.common.loaderShow();
  }

  ngOnInit() {
    this.searchService.search(this.common.config.BLOGS, this.searchTerm$,"?q=").subscribe((data: any) => {
      console.log(data);
      this.blogList = data;
    });
    this.listPopularBlogs();
    this.listBlogs();
    this.listCascadeCategories();
  }

  listPopularBlogs(){
    this.common.getService(this.common.config.POPULARBLOGS).then((res: any) => {
      this.popularList = res;
    }).catch(err => {
      console.log(err);
    })
  }

  listBlogs() {
    this.params = "?q=" + this.searchText;
    if(this.currCategory.id){
      this.params += "&subcategory__category=" + this.currCategory.id;
    }
    if(this.currSubCategory.id){
      this.params += "&subcategory=" + this.currSubCategory.id;
    }
    this.common.getService(this.common.config.BLOGS + this.params).then((res: any) => {
      console.log(res);
      this.blogList = res;
      this.common.loaderHide();
    }).catch(err => {
      this.common.loaderHide();
      console.log(err);
    })
  }

  listCascadeCategories() {
    this.common.getService(this.common.config.CASCADEARTICLES).then((res: any) => {
      console.log(res);
      this.cascadeList = res;
      this.currCategory = this.cascadeList[0];
      console.log(this.currCategory)
    }).catch(err => {
      console.log(err);
    })
  }

  selectCategory(cat) {
    this.currCategory = cat;
    this.listBlogs();
  }

  selectSubCategory(subcat) {
    this.currSubCategory = subcat;
    this.listBlogs();
  }

}
