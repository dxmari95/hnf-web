import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services';
import { ActivatedRoute } from '@angular/router';
declare var $;

@Component({
  selector: 'app-doctor-detail',
  templateUrl: './doctor-detail.component.html',
  styleUrls: ['./doctor-detail.component.scss']
})
export class DoctorDetailComponent implements OnInit {

  shopDetail: any = {
    specialization: []
  };
  shopId = "";
  reviewList: any = {};
  myreview = '';
  currTab = 1;
  relatedList = [];
  constructor(public common: GlobalService, public route: ActivatedRoute) {
    this.common.loaderShow();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.shopId = params.id
      console.log(this.shopId);
      this.listRelatedItems();
      this.retrieveShop().then(() => {
        this.initLightSlider();
      }).catch(err => { });
      this.listReviews();
    });
    this.initJS()
  }

  listRelatedItems() {
    this.common.getService(this.common.config.RELATEDDOCTOR + this.shopId).then((res: any) => {
      this.relatedList = res;
    }).catch(err => {
      console.log(err);
    })
  }


  selectTab(tab) {
    this.currTab = tab;
  }

  initJS() {
    setTimeout(() => {
      var a = $(".bg");
      a.each(function (a) {
        if ($(this).attr("data-bg")) $(this).css("background-image", "url(" + $(this).data("bg") + ")");
      });
      var self: any = window;
      self.initparallax();
    }, 500);
  }

  initLightSlider() {
    setTimeout(() => {
      function initIsotope() {
        if ($(".gallery-items").length) {
          var a = $(".gallery-items").isotope({
            singleMode: true,
            columnWidth: ".grid-sizer, .grid-sizer-second, .grid-sizer-three",
            itemSelector: ".gallery-item, .gallery-item-second, .gallery-item-three",
            transformsEnabled: true,
            transitionDuration: "700ms",
            resizable: true
          });
          a.imagesLoaded(function () {
            a.isotope("layout");
          });
        }
      }
      initIsotope();
      $(".image-popup").lightGallery({
        selector: "this",
        cssEasing: "cubic-bezier(0.25, 0, 0.25, 1)",
        download: false,
        counter: false
      });
      var o = $(".lightgallery"),
        p = o.data("looped");
      o.lightGallery({
        selector: ".lightgallery a.popup-image",
        cssEasing: "cubic-bezier(0.25, 0, 0.25, 1)",
        download: false,
        loop: false,
        counter: false
      });
      function initHiddenGal() {
        $(".dynamic-gal").on('click', function () {
          var dynamicgal = eval($(this).attr("data-dynamicPath"));

          $(this).lightGallery({
            dynamic: true,
            dynamicEl: dynamicgal,
            download: false,
            loop: false,
            counter: false
          });

        });
      }
      initHiddenGal();
    }, 200);
  }

  retrieveShop() {
    return new Promise((resolve, reject) => {
      this.common.getService(this.common.config.DOCTORS + this.shopId + "/").then(res => {
        this.shopDetail = res;
        this.common.loaderHide();
        resolve();
      }).catch(err => {
        this.common.loaderHide();
        reject(err);
      });
    })
  }

  toggleBookmark() {
    this.common.postService(this.common.config.DOCTORSBOOKMARK, { doctor: this.shopDetail.id }).then(res => {
      this.common.showSuccessToast("Bookmark added");
      this.retrieveShop();
    }).catch(err => {
      console.log(err);
    });
  }

  toggleLike() {
    this.common.postService(this.common.config.DOCTORSLIKEDISLIKE, { doctor: this.shopDetail.id }).then(res => {
      this.retrieveShop();
    }).catch(err => {
      console.log(err);
    });
  }

  goAddReview() {
    this.common.smoothMove("#sec6");
  }

  listReviews() {
    this.common.getService(this.common.config.DOCTORSREVIEWS + "?doctor=" + this.shopId).then(res => {
      console.log(res);
      this.reviewList = res;
    }).catch(err => {
      console.log(err);
    })
  }

  submitReview() {
    if (this.myreview) {
      let query = {
        "doctor": this.shopId,
        "text": this.myreview
      };
      this.common.postService(this.common.config.DOCTORSREVIEWS, query).then(res => {
        this.common.showSuccessToast("Your review has been posted.");
        this.retrieveShop();
        this.listReviews();
        this.myreview = "";
      }).catch(err => {
        console.log(err);
      });
    }
  }


}
