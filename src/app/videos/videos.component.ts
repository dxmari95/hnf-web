import { Component, OnInit } from '@angular/core';
import { GlobalService, SearchService } from '../services';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {
  videoList = {
    next: null,
    prev: null,
    count: 0,
    results: []
  };
  searchText = "";
  searchTerm$ = new Subject<string>();
  cascadeList = [];
  currCategory: any = {};
  currSubCategory: any = {};
  params = "?q=";
  popularList = [];
  constructor(public common: GlobalService, public searchService: SearchService) {
    this.common.loaderShow();
  }

  ngOnInit() {
    this.searchService.search(this.common.config.VIDEOS, this.searchTerm$,"?q=").subscribe((data: any) => {
      console.log(data);
      this.videoList = data;
    });
    this.listVideos();
    this.listCascadeCategories();
    this.listPopularVideos();
  }

  listVideos() {
    this.params = "?q=" + this.searchText;
    if(this.currCategory.id){
      this.params += "&subcategory__category=" + this.currCategory.id;
    }
    if(this.currSubCategory.id){
      this.params += "&subcategory=" + this.currSubCategory.id;
    }
    this.common.getService(this.common.config.VIDEOS + this.params).then((res: any) => {
      console.log(res);
      this.videoList = res;
      this.common.loaderHide();
    }).catch(err => {
      this.common.loaderHide();
      console.log(err);
    })
  }

  listPopularVideos(){
    this.common.getService(this.common.config.POPULARVIDEOS).then((res: any) => {
      this.popularList = res;
    }).catch(err => {
      console.log(err);
    })
  }

  listCascadeCategories() {
    this.common.getService(this.common.config.CASCADEARTICLES).then((res: any) => {
      console.log(res);
      this.cascadeList = res;
      this.currCategory = this.cascadeList[0];
      console.log(this.currCategory)
    }).catch(err => {
      console.log(err);
    })
  }

  selectCategory(cat) {
    this.currCategory = cat;
    this.listVideos();
  }

  selectSubCategory(subcat) {
    this.currSubCategory = subcat;
    this.listVideos();
  }
}
