import { Component, OnInit } from '@angular/core';
import { GlobalService } from './services';
import { ToasterConfig } from 'angular2-toaster';
declare var $;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  loginDetails: any = {};
  registerDetails: any = {};
  loginValidator: any = {};
  registerValidator: any = {};
  OTP;
  resetDetails: any = {};
  loginOTPDetails: any = {};
  forgotDetails: any = {};
  isLoading = false;
  formType = 1;
  public config: ToasterConfig = new ToasterConfig({
    animation: "fade",
    positionClass: "toast-bottom-center",
    timeout: 3000,
    preventDuplicates: true
  });

  constructor(public common: GlobalService) {

  }

  ngOnInit() {
    setTimeout(() => {
      $(() => {
        $(".enterotpsection").hide();
        $(".forgotpwdsection").hide();
        $(".resetpwdsection").hide();
        $(".loginwithotpsection").hide();
        $(".enterotptologinsection").hide();
      });
      this.loginValidator = $(".login-form").validate({
        rules: {
          email: {
            required: true
          },
          password: {
            required: true,
            minlength: 6
          }
        },
        messages: {
          password: {
            minlength: "Please enter at least 5 characters"
          }
        },
        errorElement: "div",
        errorPlacement: function (error, element) {
          var placement = $(element).data("error");
          if (placement) {
            $(placement).append(error);
          } else {
            error.insertAfter(element);
          }
        },
        submitHandler: e => {
          console.log(e);
          setTimeout(() => {
            this.login(this.loginDetails);
          }, 500);
          return false;
        }
      });
      console.log(this.loginValidator);

      this.registerValidator = $(".register-form").validate({
        rules: {
          first_name: {
            required: true
          },
          email: {
            required: true
          },
          mobile: {
            required: true,
            minlength: 10,
            maxlength: 10
          },
          password: {
            required: true,
            minlength: 6
          }
        },
        messages: {
          password: {
            minlength: "Please enter at least 5 characters"
          },
          mobile: {
            minlength: "Mobile number should be 10 digits exactly",
            maxlength: "Mobile number should be 10 digits exactly",
          }
        },
        errorElement: "div",
        errorPlacement: function (error, element) {
          var placement = $(element).data("error");
          if (placement) {
            $(placement).append(error);
          } else {
            error.insertAfter(element);
          }
        },
        submitHandler: e => {
          console.log(e);
          setTimeout(() => {
            this.register(this.registerDetails);
          }, 500);
          return false;
        }
      });

      $(".reset-form").validate({
        rules: {
          reset_otp: {
            required: true,
            minlength: 4,
            maxlength: 4
          },
          password: {
            required: true,
            minlength: 5
          },
          new_password: {
            required: true,
            minlength: 5,
            equalTo: "#password"
          }
        },
        messages: {
          old_password: {
            minlength: "Please enter at least 5 characters"
          },
          new_password: {
            minlength: "Please enter at least 5 characters"
          },
          otp: {
            minlength: "OTP should be 4 digits exactly",
            maxlength: "OTP should be 4 digits exactly",
          }
        },
        errorElement: "div",
        errorPlacement: function (error, element) {
          var placement = $(element).data("error");
          if (placement) {
            $(placement).append(error);
          } else {
            error.insertAfter(element);
          }
        },
        submitHandler: e => {
          console.log(e);
          setTimeout(() => {
            this.resetPassword(this.resetDetails);
          }, 500);
          return false;
        }
      });

      $(".otp-form").validate({
        rules: {
          otp: {
            required: true,
            minlength: 4,
            maxlength: 4
          }
        },
        messages: {
          otp: {
            minlength: "OTP should be 4 digits exactly",
            maxlength: "OTP should be 4 digits exactly",
          }
        },
        errorElement: "div",
        errorPlacement: function (error, element) {
          var placement = $(element).data("error");
          if (placement) {
            $(placement).append(error);
          } else {
            error.insertAfter(element);
          }
        },
        submitHandler: e => {
          console.log(e);
          setTimeout(() => {
            this.verifyOTP(this.OTP);
          }, 500);
          return false;
        }
      });

      $(".login-otp").validate({
        rules: {
          login_phone: {
            required: true,
            minlength: 10,
            maxlength: 10
          }
        },
        messages: {
          login_phone: {
            minlength: "Mobile Number should be 10 digits exactly",
            maxlength: "Mobile Number should be 10 digits exactly",
          }
        },
        errorElement: "div",
        errorPlacement: function (error, element) {
          var placement = $(element).data("error");
          if (placement) {
            $(placement).append(error);
          } else {
            error.insertAfter(element);
          }
        },
        submitHandler: e => {
          console.log(e);
          setTimeout(() => {
            this.loginWithOTP();
          }, 500);
          return false;
        }
      });

      $(".forgot-form").validate({
        rules: {
          login_phone: {
            required: true,
            minlength: 10,
            maxlength: 10
          }
        },
        messages: {
          login_phone: {
            minlength: "Mobile Number should be 10 digits exactly",
            maxlength: "Mobile Number should be 10 digits exactly",
          }
        },
        errorElement: "div",
        errorPlacement: function (error, element) {
          var placement = $(element).data("error");
          if (placement) {
            $(placement).append(error);
          } else {
            error.insertAfter(element);
          }
        },
        submitHandler: e => {
          console.log(e);
          setTimeout(() => {
            this.sendOTP(this.forgotDetails, true);
          }, 500);
          return false;
        }
      });

      $(".otp_login").validate({
        rules: {
          otp: {
            required: true,
            minlength: 4,
            maxlength: 4
          }
        },
        messages: {
          otp: {
            minlength: "OTP should be 4 digits exactly",
            maxlength: "OTP should be 4 digits exactly",
          }
        },
        errorElement: "div",
        errorPlacement: function (error, element) {
          var placement = $(element).data("error");
          if (placement) {
            $(placement).append(error);
          } else {
            error.insertAfter(element);
          }
        },
        submitHandler: e => {
          console.log(e);
          setTimeout(() => {
            this.verifyOTP(this.OTP);
          }, 500);
          return false;
        }
      });

      console.log(this.registerValidator);

    }, 700);

  }

  loginWithOTP() {
    this.isLoading = true;
    let query = { username: this.loginOTPDetails.mobile };
    this.common.postService(this.common.config.PASSWORDLESSLOGIN, query).then((res: any) => {
      this.common.showSuccessToast("OTP was send to your registered mobile number.");
      this.registerDetails.uid = res.id;
      this.forgotDetails.forgot_uname = this.loginOTPDetails.mobile;
      this.isLoading = false;
      this.enterotptologin();
    }).catch(err => {
      console.log(err);
      this.isLoading = false;
      this.common.showErrorToast('', err);
    })
  }

  verifyOTP(OTP) {
    console.log(OTP);
    let query = {
      "uid": this.registerDetails.uid,
      "otp": OTP,
      "client": this.common.generateUUID()
    };
    this.isLoading = true;
    this.common.postService(this.common.config.ACTIVATEACCOUNT, query).then(res => {
      this.common.go('/');
      console.log(res);
      this.isLoading = false;
      // $('.modal').hide();
      $('.modal , .reg-overlay').fadeOut(200);
      $("html, body").removeClass("hid-body");
      localStorage.setItem('hnf_web', JSON.stringify(res));
      this.common.authData = res;
    }).catch(err => {
      console.log(err);
      this.isLoading = false;
      this.common.showErrorToast("", err);
    })
  }


  enterotpsection() {
    $(".registersection").hide();
    $(".enterotpsection").show();
  }

  clickregister() {
    $(".registersection").show();
    $(".enterotpsection").hide();
  }

  clickforgotpwd() {
    $(".forgotpwdsection").show();
    $(".loginsection").hide();
  }

  clicklogin() {
    $(".forgotpwdsection").hide();
    $(".resetpwdsection").hide();
    $(".loginsection").show();
  }

  clickloginwithotp() {
    $(".loginwithotpsection").show();
    $(".loginsection").hide();
  }

  enterotptologin() {
    $(".enterotptologinsection").show();
    $(".loginwithotpsection").hide();
  }

  clickForgot() {
    $(".forgotpwdsection").hide();
    $(".resetpwdsection").show();
  }

  backtologpwd() {
    $(".enterotptologinsection").hide();
    $(".loginwithotpsection").hide();
    $(".loginsection").show();
  }


  login(loginParams) {
    console.log(loginParams);
    let query = {
      "username": loginParams.username,
      "password": loginParams.password,
      "client": this.common.generateUUID()
    };
    this.isLoading = true;
    this.common.postService(this.common.config.LOGIN, query).then(res => {
      this.common.go('/');
      console.log(res);
      this.isLoading = false;
      // $('.modal').hide();
      $('.modal , .reg-overlay').fadeOut(200);
      $("html, body").removeClass("hid-body");
      localStorage.setItem('hnf_web', JSON.stringify(res));
      this.common.authData = res;
    }).catch(err => {
      console.log(err);
      this.isLoading = false;
      if (err.status == 406) {
        this.enterotpsection();
      } else {
        this.common.showErrorToast("", err);
      }
    })
  }


  register(registerParams) {
    console.log(registerParams);
    this.registerDetails.errors = {};
    let query = {
      "first_name": registerParams.first_name,
      "last_name": registerParams.last_name || '',
      "username": registerParams.mobile,
      "email": registerParams.email,
      "password": registerParams.password
    };
    this.isLoading = true;
    this.common.postService(this.common.config.REGISTER, query).then((res: any) => {
      console.log(res);
      this.formType = 5;
      this.isLoading = false;
      this.common.showSuccessToast("OTP was send to your registered mobile number.");
      this.registerDetails.uid = res.id;
      this.forgotDetails.forgot_uname = this.registerDetails.mobile;
      this.enterotpsection();
    }).catch(err => {
      this.isLoading = false;
      console.log(err);
      if (err.status == 406) {
        this.enterotpsection();
      } else {
        if (err.non_field_errors) {
          this.common.showErrorToast('', err);
        } else {
          this.registerDetails.errors = err;
        }
      }
    })
  }

  changeType(type) {
    this.formType = type;
  }

  sendOTP(forgot, isForgot?) {
    console.log(forgot);
    this.clickForgot();
    return;
    this.isLoading = true;
    let query = { username: forgot.forgot_uname };
    this.common.postService(this.common.config.FORGOTPWD, query).then((res: any) => {
      this.formType = 4;
      this.common.showSuccessToast("OTP was send to your registered mobile number.");
      this.resetDetails.uid = res.id;
      if (isForgot) {
        this.registerDetails.uid = res.id;
        this.forgotDetails.forgot_uname = forgot.forgot_uname;
        this.clickForgot();
      }
      this.isLoading = false;
    }).catch(err => {
      console.log(err);
      this.isLoading = false;
      this.common.showErrorToast('', err);
    })
  }

  resetPassword(reset) {
    console.log(reset);
    let query: any = {
      "uid": this.resetDetails.uid,
      "otp": reset.otp,
      "client": this.common.generateUUID()
    }, url = '';

    if (this.formType == 4) {
      query.password = reset.new_password;
      url = this.common.config.RESETPWD;
    } else {
      url = this.common.config.ACTIVATEACCOUNT;
    }
    this.isLoading = true;
    this.common.postService(url, query).then(res => {
      this.common.go('/');
      console.log(res);
      localStorage.setItem('hnf_web', JSON.stringify(res));
      this.common.authData = res;
      this.isLoading = false;
    }).catch(err => {
      this.isLoading = false;
      console.log(err);
      this.common.showErrorToast('', err);
    })
  }

  facebookSignIn() {
    this.common.facebookSignIn().then(res => {
      console.log(res);
    }).catch(err => {
      console.log(err);
      this.common.showErrorToast("Unable to login please try again..!!");
    });
  }

  googleSignIn() {
    this.common.googleSignIn().then(res => {
      console.log(res);
      localStorage.setItem('googleAuth',JSON.stringify(res));
    }).catch(err => {
      console.log(err);
      this.common.showErrorToast("Unable to login please try again..!!");
    });
  }
}
