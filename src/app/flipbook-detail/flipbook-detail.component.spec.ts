import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlipbookDetailComponent } from './flipbook-detail.component';

describe('FlipbookDetailComponent', () => {
  let component: FlipbookDetailComponent;
  let fixture: ComponentFixture<FlipbookDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlipbookDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlipbookDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
