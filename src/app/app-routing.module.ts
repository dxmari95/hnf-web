import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { BlogsComponent } from './blogs/blogs.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { VideosComponent } from './videos/videos.component';
import { FlipbooksComponent } from './flipbooks/flipbooks.component';
import { VideoDetailComponent } from './video-detail/video-detail.component';
import { FlipbookDetailComponent } from './flipbook-detail/flipbook-detail.component';
import { ShopsComponent } from './shops/shops.component';
import { ShopDetailComponent } from './shop-detail/shop-detail.component';
import { FitnessComponent } from './fitness/fitness.component';
import { FitnessDetailComponent } from './fitness-detail/fitness-detail.component';
import { PharmaciesComponent } from './pharmacies/pharmacies.component';
import { PharmaciesDetailComponent } from './pharmacies-detail/pharmacies-detail.component';
import { DoctorsComponent } from './doctors/doctors.component';
import { DoctorDetailComponent } from './doctor-detail/doctor-detail.component';
import { ClinicsComponent } from './clinics/clinics.component';
import { ClinicDetailComponent } from './clinic-detail/clinic-detail.component';
import { ForumsComponent } from './forums/forums.component';
import { ForumDetailComponent } from './forum-detail/forum-detail.component';
import { ViewFlipbookComponent } from './view-flipbook/view-flipbook.component';

const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: '', component: HomeComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'blogs', component: BlogsComponent },
  { path: 'blog/:id', component: BlogDetailComponent },
  { path: 'videos', component: VideosComponent },
  { path: 'video/:id', component: VideoDetailComponent },
  { path: 'flipbooks', component: FlipbooksComponent },
  { path: 'flipbook/:id', component: FlipbookDetailComponent },
  { path: 'view-flipbook/:id', component: ViewFlipbookComponent },
  { path: 'shops', component: ShopsComponent },
  { path: 'forums', component: ForumsComponent },
  { path: 'forum-detail/:id', component: ForumDetailComponent },
  { path: 'shop-detail/:id', component: ShopDetailComponent },
  { path: 'fitness-centers', component: FitnessComponent },
  { path: 'fitness-detail/:id', component: FitnessDetailComponent },
  { path: 'pharmacies', component: PharmaciesComponent },
  { path: 'pharmacy-detail/:id', component: PharmaciesDetailComponent },
  { path: 'doctors', component: DoctorsComponent },
  { path: 'doctor-detail/:id', component: DoctorDetailComponent },
  { path: 'clinics', component: ClinicsComponent },
  { path: 'clinic-detail/:id', component: ClinicDetailComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
