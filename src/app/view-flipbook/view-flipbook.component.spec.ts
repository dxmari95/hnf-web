import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFlipbookComponent } from './view-flipbook.component';

describe('ViewFlipbookComponent', () => {
  let component: ViewFlipbookComponent;
  let fixture: ComponentFixture<ViewFlipbookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewFlipbookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewFlipbookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
