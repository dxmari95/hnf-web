import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-forum-detail',
  templateUrl: './forum-detail.component.html',
  styleUrls: ['./forum-detail.component.scss']
})
export class ForumDetailComponent implements OnInit {
  forumId;
  forumDetail: any = {};
  reviewList: any = {};
  myreview = '';

  constructor(public common: GlobalService, public route: ActivatedRoute) {
    this.common.loaderShow();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.forumId = params.id
      console.log(this.forumId);
      this.retrieveForum().then(() => {
      }).catch(err => { });
    });
  }

  retrieveForum() {
    return new Promise((resolve, reject) => {
      this.common.getService(this.common.config.RETRIEVEFORUMS + this.forumId).then(res => {
        this.forumDetail = res;
        this.reviewList = this.forumDetail.comments;
        this.common.loaderHide();
        resolve();
      }).catch(err => {
        this.common.loaderHide();
        reject(err);
      });
    })
  }

  goAddReview() {
    this.common.smoothMove("#sec6");
  }

  submitReview() {
    if (this.myreview) {
      let query = {
        "forum": this.forumId,
        "comment": this.myreview
      };
      this.common.postService(this.common.config.CREATECOMMENTS, query).then(res => {
        this.common.showSuccessToast("Your comment has been posted.");
        this.retrieveForum();
        this.myreview = "";
      }).catch(err => {
        console.log(err);
      });
    }
  }


}
