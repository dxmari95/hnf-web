import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitnessDetailComponent } from './fitness-detail.component';

describe('FitnessDetailComponent', () => {
  let component: FitnessDetailComponent;
  let fixture: ComponentFixture<FitnessDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitnessDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitnessDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
