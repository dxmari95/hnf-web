import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services';
declare var $;
@Component({
  selector: 'app-forums',
  templateUrl: './forums.component.html',
  styleUrls: ['./forums.component.scss']
})
export class ForumsComponent implements OnInit {
  forumsCategory: any = [];
  currCategory: any = {};
  profileDetails: any = {};
  question: any = {};
  forumsList: any = [];

  constructor(public common: GlobalService) {
    this.common.loaderShow();
  }

  ngOnInit() {
    this.retrieveProfile();
    this.listCategories();
    setTimeout(() => {
      $('#gender').niceSelect();
    })
    this.listQuestions();
  }

  listCategories() {
    this.common.getService(this.common.config.FORUMSCATEGORY).then(res => {
      this.forumsCategory = res;
    }).catch(err => {
    });
  }

  retrieveProfile() {
    this.common.getService(this.common.config.PROFILE).then((res: any) => {
      this.profileDetails = {
        "name": res.first_name + (res.last_name ? (" " + res.last_name) : ''),
        "email": res.email,
        "username": res.username,
        "age": this.calculateAge(res.userprofile.dob),
        "userprofile": {
          "dob": res.userprofile.dob,
          "gender": res.userprofile.gender,
        }
      }
      console.log(this.profileDetails);
      if (this.profileDetails.userprofile) {
        $('#gender').val(this.profileDetails.userprofile.gender);
        $('#gender').niceSelect('update');
      }
    }).catch(err => {
      console.log(err);
    })
  }

  selectCategory(category) {
    this.currCategory = category;
  }

  listQuestions() {
    this.common.getService(this.common.config.QUESTIONS).then(res => {
      this.forumsList = res;
      this.common.loaderHide();
    }).catch(err => {
      this.common.loaderHide();
    })
  }

  showmyselfsec() {
    $(".myself-sec").show(500);
    $(".someone-sec").hide(500);
  }

  showsomeonesec() {
    $(".myself-sec").hide(500);
    $(".someone-sec").show(500);
  }

  hidebothqstnsec() {
    $(".myself-sec").hide(300);
    $(".someone-sec").hide(300);
  }

  calculateAge(birthDate, otherDate = new Date()) {
    birthDate = new Date(birthDate);
    otherDate = new Date(otherDate);

    var years = (otherDate.getFullYear() - birthDate.getFullYear());

    if (otherDate.getMonth() < birthDate.getMonth() ||
      otherDate.getMonth() == birthDate.getMonth() && otherDate.getDate() < birthDate.getDate()) {
      years--;
    }

    return years;
  }

  createQuestion() {
    this.question.titleErr = !this.question.title;
    this.question.descErr = !this.question.description;
    if (this.question.title && this.question.description) {
      this.common.postService(this.common.config.QUESTIONS, {
        category_id: this.currCategory.id,
        title: this.question.title,
        description: this.question.description
      }).then(res => {
        console.log(res);
        this.question = {};
        this.common.showSuccessToast("Your question was posted successfully.");
      }).catch(err => {
        this.common.showErrorToast('', err);
      });
    }
  }

  // createComment(){
  //   this.common.postService(this.common.config.CREATECOMMENTS,{
  //     // forum : this.
  //   }).then(res =>{
  //     console.log(res);
  //     this.common.showSuccessToast("Your question was posted successfully.");
  //   });
  // }

}
